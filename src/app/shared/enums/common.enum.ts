import { EnumsHelper } from '@shared/common/enums.helper';

export class CommonEnums extends EnumsHelper {
    public sortType = sortType;
    public sortTypeDisplay = sortTypeDisplay;

    public getNames(key: string): string[] {
        return super.getNames(this[key]);
    }

    public getDisplay(key: string, displayKey: string): { label: string, value: string }[] {
        return super.getDisplay(this[key], this[displayKey]);
    }

    public getSingleDisplay(key: string, value: string, displayKey: string): string {
        return this[displayKey][this[key][value]];
    }

    public getDisplayValue(key: string, e: string): string {
        const index = this[key][e];
        return this[key + 'Display'][index];
    }
}

enum sortType {
    PRICE,
    SCORE,
    ALPHABETICAL
}

enum sortTypeDisplay {
    'Preço',
    'Mais Populares',
    'Ordem Alfabética'
}
