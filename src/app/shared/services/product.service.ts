import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { AppConfig } from '@core/config/app.config.interface';
import { APP_CONFIG } from '@core/config/app.config.constant';
import { Product } from '@shared/models/product.model';

@Injectable()
export class ProductService {
    
    public cartProducts: Product[] = [];
    private cartProductsSubject: Subject<Product> = new Subject();
	private cartProductsObservable: Observable<Product> = this.cartProductsSubject.asObservable();

    /**
     * Creates an instance of ProductService.
     * @param {HttpClient} http
     * @memberof ProductService
     */
    constructor(
        private http: HttpClient,
        @Inject(APP_CONFIG)
        private config: AppConfig
    ) { }

     /**
     * Returns the products in the cart
     *
     * @readonly
     * @returns {Product[]}
     * @type {Product}
     */	
    public getProductsList(): Product[] {
        return this.cartProducts;
    }


    /**
     * Get the products added to the cart
     *
     * @returns {Observable<Product>}
     * @memberof ProductService
     */
    public getCartSummary(): Observable<Product> {
        return this.cartProductsObservable;
    }

    /**
     * Adds a product to the cart list
     *
     * @param {Product} product
     * @returns {void}
     * @memberof ProductService
     */
    public addProductToCart(product: Product): void {
        this.cartProducts.push(product);
        this.cartProductsSubject.next(product);
    }

    /**
     * Removes a product from the product list
     *
     * @returns {void}
     * @param {Product} product
     * @memberof ProductService
     */
    public removeProductFromCart(product: Product): void {
        const index = this.cartProducts.indexOf(product);
        if (index !== -1) {
            this.cartProducts.splice(index, 1);
            this.cartProductsSubject.next();
        }
    }

    /**
     * Gets the list of products
     *
     * @returns {Observable<Product[]>}
     * @memberof ProductService
     */
    public getProducts(): Observable<Product[]> {
        const url = this.config.API.PRODUCT.GET()
        return this.http.get<Product[]>(url);
    }
}
