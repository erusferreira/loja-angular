export class EnumsHelper {

    constructor() {}

    public getNames(obj: any): Array<any> {
        return this.getObjValues(obj).filter(v => typeof v === 'string') as string[];
    }

    public getValues<T extends number>(obj: any): Array<any> {
        return this.getObjValues(obj).filter(v => typeof v === 'number') as T[];
    }

    public getDisplay (obj: any, displayObj: any): Array<any> {
        const display: Array<any> = [];
        for (let i = 0; i < Object.keys(obj).length; i++) {
            if (obj[i] !== undefined) {
                display.push({
                    label: displayObj[i],
                    value: obj[i]
                });
            }
        }
        return display;
    }

    private getObjValues(obj: any) {
        return Object.keys(obj).map(k => obj[k]);
    }
}
