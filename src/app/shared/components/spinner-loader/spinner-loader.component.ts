import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinner-loader',
  template: `
    <div></div>
  `,
  styleUrls: ['./spinner-loader.component.scss'],
})
export class SpinnerLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
