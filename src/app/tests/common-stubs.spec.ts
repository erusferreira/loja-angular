import { HttpResponse, HttpClient } from '@angular/common/http';
import { of as ObservableOf } from 'rxjs';
import { APP_CONFIG } from '@core/config/app.config.constant';

export const StubAppConfig = (config: string[] = []) => ({
    provide: APP_CONFIG,
    useValue: {
        APP_NAME: 'Game Store',
        API: Object.assign({}, (() => {
            return config.reduce((map: any, prev: string) => {
                map[prev] = {};
                return map;
            }, {});
        })())
    }
});

export const StubHttpService = (requestData?: any) => {
    const spyByVerb = verb => {
        const options = {
            body: Object.assign({}, requestData),
        };
        const response = new HttpResponse(options);
        const response$ = ObservableOf(response);
        return jasmine.createSpy(verb).and.returnValue(response$);
    };
    return {
        provide: HttpClient,
        useValue: {
            get: spyByVerb('get'),
            post: spyByVerb('post'),
            put: spyByVerb('put'),
            delete: spyByVerb('delete'),
            patch: spyByVerb('patch')
        }
    };
};
