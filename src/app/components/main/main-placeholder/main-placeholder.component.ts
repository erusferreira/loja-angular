import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-main-placeholder',
	templateUrl: './main-placeholder.component.html',
	styleUrls: ['./main-placeholder.component.scss']
})
export class MainPlaceholderComponent implements OnInit {
	
	public products: any[];
	private numberOfProducts: number = 9;
	
	constructor() { }
	
	/**
	* Life cycle hook called by @angular
	* @returns {void}
	* @memberof MainComponent
	*/
	public ngOnInit(): void {
		this.products = Array.from(new Array(this.numberOfProducts), (val,index) => index + 1 );
	}
	
}
