import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPlaceholderComponent } from './main-placeholder.component';

describe('MainPlaceholderComponent', () => {
  let component: MainPlaceholderComponent;
  let fixture: ComponentFixture<MainPlaceholderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPlaceholderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
