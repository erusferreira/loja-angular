import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';

import { LayoutModule } from '@shared/layout/layout.module';
import { ProductService } from '@shared/services/product.service';
import { MainComponent } from './main.component';
import { ProductComponent } from '../product/product.component';
import { CartComponent } from '../cart/cart.component';
import { CartProductComponent } from '../cart-product/cart-product.component';
import { CartIconComponent } from '@shared/components/cart-icon/cart-icon.component';
import { SpinnerLoaderComponent } from '@shared/components/spinner-loader/spinner-loader.component';
import { MainPlaceholderComponent } from './main-placeholder/main-placeholder.component';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    ReactiveFormsModule,
    LayoutModule,
    DropdownModule
  ],
  declarations: [
    MainComponent,
    ProductComponent,
    CartComponent,
    CartProductComponent,
    CartIconComponent,
    SpinnerLoaderComponent,
    MainPlaceholderComponent
  ],
  providers: [
    ProductService
  ]
})
export class MainModule { }
