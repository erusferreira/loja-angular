import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SelectItem } from 'primeng/api';

import { CommonEnums } from '@shared/enums/common.enum';
import { Product } from '@shared/models/product.model';
import { ProductService } from '@shared/services/product.service';

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss'],
	providers: [CommonEnums]
})
export class MainComponent implements OnInit {

	public sortList: SelectItem[];
	public products: Array<Product>;
	public sortForm: FormGroup;
	public hasProducts: boolean;
	public productsAddedToCart: number;
	private subscriptionDestroyer: Subject<any> = new Subject();

	constructor(
		public enums: CommonEnums,
		private formBuilder: FormBuilder,
		private productService: ProductService
	) { }

	/**
	 * Life cycle hook called by @angular
	 * @returns {void}
	 * @memberof MainComponent
	 */
	public ngOnInit(): void {
		this.productService.getProducts()
			.pipe(takeUntil(this.subscriptionDestroyer))
			.subscribe(response => {
				this.products = response;
				this.buildForm();
			});
		this.getProductsOfCart();
	}

    /**
     * Unsubscribes from watchers
     * @returns {void}
     * @memberof MainComponent
     */
    public ngOnDestroy(): void {
        this.subscriptionDestroyer.next();
        this.subscriptionDestroyer.complete();
	}

	/**
	 * Sort the product list accordingly to the selected form option
	 * @returns {void}
	 * @memberof MainComponent
	 */
	public applySort(): void {
		this.products.sort((a, b) => {
			if (this.sortControl === this.enums.sortType[this.enums.sortType.ALPHABETICAL]) {
				const order = a.name < b.name ? -1 : 1;
           		return order || 0;
			} else if (this.sortControl === this.enums.sortType[this.enums.sortType.PRICE]) {
				const order = a.price < b.price ? -1 : 1;
            	return order || 0;
			} else if (this.sortControl === this.enums.sortType[this.enums.sortType.SCORE]) {
				const order = a.score < b.score ? 1 : -1;
            	return order || 0;
			}
		});
	}

	/**
	 * Get the products added to the cart
	 * @param {*} $event
	 * @memberof MainComponent
	 */
	public getProductsOfCart(): void {
			this.productService.getCartSummary()
				.pipe(takeUntil(this.subscriptionDestroyer))
				.subscribe(() => {
					this.productsAddedToCart = this.productService.getProductsList().length;
				});
	}

	/**
	 * Returns the sort control value
	 * @readonly
	 * @private
	 * @type {string}
	 * @memberof MainComponent
	 */
	private get sortControl(): string {
		return this.sortForm.get('sort').value;
	}

	/**
	 * This method builds the sort form
	 * @private
	 * @returns {void}
	 * @memberof MainComponent
	 */
	private buildForm(): void {
		this.sortList = [{
			label: 'Ordenar',
			value: null
		}];
		this.enums.getNames('sortType')
			.forEach((response) => {
				const option: SelectItem = {
					label: this.enums.getSingleDisplay('sortType', response, 'sortTypeDisplay'),
					value: response
				}
				this.sortList.push(option);
			});

		this.sortForm = this.formBuilder.group({
			sort: []
		});
		
		this.hasProducts = true;
		this.applySort();
	}

}
