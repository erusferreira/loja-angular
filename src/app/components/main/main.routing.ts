import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MainComponent } from './main.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MainComponent
            },
            {
                path: '**',
                redirectTo: '/',
                pathMatch: 'full'
            } 
        ])
    ]
})
export class MainRoutingModule { }
