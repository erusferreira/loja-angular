import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup,  FormArray } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { HttpClientModule } from '@angular/common/http';

import { LayoutModule } from '@shared/layout/layout.module';
import { ProductService } from '@shared/services/product.service';
import { MainComponent } from './main.component';
import { ProductComponent } from '../product/product.component';
import { CartComponent } from '../cart/cart.component';
import { CartProductComponent } from '../cart-product/cart-product.component';
import { CartIconComponent } from '@shared/components/cart-icon/cart-icon.component';
import { SpinnerLoaderComponent } from '@shared/components/spinner-loader/spinner-loader.component';
import { MainPlaceholderComponent } from './main-placeholder/main-placeholder.component';
import { CommonEnums } from '@shared/enums/common.enum';
import { APP_CONFIG, APP_DI_CONFIG } from '@core/config/app.config.constant';
import { StubAppConfig, StubHttpService } from '../../tests/common-stubs.spec';

fdescribe('MainComponent', () => {
  let component: MainComponent;
  let fixture: ComponentFixture<MainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MainComponent,
        ProductComponent,
        CartComponent,
        CartProductComponent,
        CartIconComponent,
        SpinnerLoaderComponent,
        MainPlaceholderComponent
      ],
      providers: [
        FormBuilder,
        ProductService,
        CommonEnums,
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG
        },
        // StubAppConfig(),
        // StubHttpService(),
      ],
      imports: [
        HttpClientModule,
        LayoutModule,
        CommonModule,
        MainRoutingModule,
        ReactiveFormsModule,
        DropdownModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
