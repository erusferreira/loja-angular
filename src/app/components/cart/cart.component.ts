import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Product } from '@shared/models/product.model';
import { ProductService } from '@shared/services/product.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
	
	public products: Array<Product>;
	public subtotal: number;
	public shipping: number;
	public shippingLimitPrice: number = 250;
	public total: number;
	private subscriptionDestroyer: Subject<any> = new Subject();
	
	constructor(
		private productService: ProductService
	) { }
	
	/**
	 * Life cycle hook called by @angular
	 * @returns {void}
	 * @memberof CartComponent
	 */
	public ngOnInit(): void {
		this.products = this.productService.getProductsList();
		this.getSummaryData();
	}
	
	public getSummaryData(): void {
		this.productService.getCartSummary()
			.pipe(takeUntil(this.subscriptionDestroyer))
			.subscribe(() => {
				this.subtotal = this.products.reduce((total, current) => (total + current.price), 0);
				this.shipping = this.subtotal > this.shippingLimitPrice ? 0 : this.products.length * 10;
				this.total = this.subtotal + this.shipping;
			});
	}

	/**
	 * Has products added to the cart?
	 * @readonly
	 * @type {boolean}
	 * @memberof CartComponent
	 */
	public get hasProducts(): boolean {
		return this.products.length > 0;
	}

	/**
	 * Has a single product been added to the cart?
	 * @readonly
	 * @type {boolean}
	 * @memberof CartComponent
	 */
	public get hasSingleProduct(): boolean {
		return this.products.length === 1;
	}

    /**
     * Unsubscribes from watchers
     * @returns {void}
     * @memberof CartComponent
     */
    public ngOnDestroy(): void {
        this.subscriptionDestroyer.next();
        this.subscriptionDestroyer.complete();
	}
	
}
