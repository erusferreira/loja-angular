import { Component, OnInit, Input } from '@angular/core';

import { Product } from '@shared/models/product.model';
import { ProductService } from '@shared/services/product.service';

@Component({
  selector: 'app-cart-product',
  templateUrl: './cart-product.component.html',
  styleUrls: ['./cart-product.component.scss']
})
export class CartProductComponent implements OnInit {

	@Input() public product: Product;

	constructor(
		private productService: ProductService
	) { }
	
	/**
	 * Life cycle hook called by @angular
	 * @returns {void}
	 * @memberof MainComponent
	 */
	public ngOnInit(): void {
	}

	/**
	 * Removes the product from the cart
	 * @returns {void}
	 * @param {Product} product
	 * @memberof CartProductComponent
	 */
	public removeProduct(product: Product): void {
		this.productService.removeProductFromCart(product);
	}

}
