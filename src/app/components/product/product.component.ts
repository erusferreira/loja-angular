import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Product } from '@shared/models/product.model';
import { ProductService } from '@shared/services/product.service';

@Component({
	selector: 'app-product',
	templateUrl: './product.component.html',
	styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnChanges {
	
	public isMouseOver: boolean;
	public hasProduct: boolean = false;
	public hasImagesLoaded: boolean = false;
	@Input() public product: Product;

	constructor(
		private productService: ProductService
	) { }
	
	/**
	 * Life cycle hook by @angular
	 *
	 * @memberof ProductComponent
	 */
	public ngOnInit() {
	}

	/**
	 * Check if the mouse is over the product and if there's a product added to the cart
	 * 
	 * @returns {void}
	 * @param {boolean} isMouseOverProduct
	 * @param {Product} [product]
	 * @memberof ProductComponent
	 */
	public checkProduct(isMouseOverProduct: boolean, product?: Product): void {
		this.hasProduct = this.productService.getProductsList().includes(product);
		this.isMouseOver = isMouseOverProduct;
	}

	/**
	 * Toggle add or remove product from cart
	 * 
	 * @returns {void}
	 * @param {Product} product
	 * @memberof ProductComponent
	 */
	public toggleProductToCart(product: Product): void {
		this.hasProduct ? this.productService.removeProductFromCart(product) : this.productService.addProductToCart(product);
		this.hasProduct = !this.hasProduct;
	}

	/**
	 * Life cycle hook by @angular
	 *
	 * @memberof ProductComponent
	 */
	public ngOnChanges(changes: SimpleChanges) {
		if (
			changes.product && 
			changes.product.currentValue && 
			changes.product.currentValue.image
		) {
			this.hasImagesLoaded = true;
		}
	  }
	
}
