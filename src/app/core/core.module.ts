import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { APP_CONFIG, APP_DI_CONFIG } from '@core/config/app.config.constant';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule
    ],
    exports: [
    ],
    providers: [
        {
            provide: APP_CONFIG,
            useValue: APP_DI_CONFIG
        }
    ]
})
export class CoreModule {
    constructor() { }
}
