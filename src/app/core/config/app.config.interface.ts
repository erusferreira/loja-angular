export interface AppConfig {
    APP_NAME: string;
    API: any;
}

export interface AppAPIConfig {
    GET_ALL?: any;
    GET?: any;
    MOCK_GET?: any;
    DELETE?: any;
    POST?: any;
    PUT?: any;
    PATCH?: any;
}
