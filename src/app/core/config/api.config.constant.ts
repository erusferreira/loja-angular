import { AppAPIConfig } from './app.config.interface';

/**
 * API Config
 *
 * API config contains a constant, which contains properties
 * that relate to each feature and/or resource. Each property is
 * cast to the `AppAPIConfig` type which contains properties
 * for RESTful methods.
 *
 * Remember to keep this AOT compliant
 *
 */
export const APIConfig = {
    PRODUCT: <AppAPIConfig>{
        GET: () =>
            `assets/products.json`
    }

}
