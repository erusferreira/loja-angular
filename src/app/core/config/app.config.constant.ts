import { InjectionToken } from '@angular/core';
import { AppConfig } from './app.config.interface';
import { APIConfig } from './api.config.constant';
/**
 * App Config
 *
 * The app config is defined as non-class dependency,
 * which makes use of the InjectionToken class. This
 * allows us to register the APP_CONFIG constant as
 * an Angular dependency provider. This is the current
 * documented solution for creating injectable constant
 * configs.
 */
export const APP_DI_CONFIG: AppConfig = {
    APP_NAME: 'Game Store',
    API: APIConfig
};

export let APP_CONFIG = new InjectionToken<AppConfig>('app.config');
